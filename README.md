# trivy-db-glad

Build pipeline for [Trivy](https://github.com/aquasecurity/trivy) databases that
contain the [GitLab Advisory Database](https://gitlab.com/gitlab-org/security-products/gemnasium-db).

## Pulling a database artifact

Databases are stored as OCI artifacts in the container registry and can be
pulled with e.g. the [ORAS CLI](https://oras.land/):

```
oras pull -a registry.gitlab.com/gitlab-org/security-products/dependencies/trivy-db-glad:2
```

## Available tags

Mirroring [trivy-db](https://github.com/aquasecurity/trivy-db)'s tagging scheme,
the latest database artifact is tagged with `:2`. This database gets pulled into
[container-scanning](https://gitlab.com/gitlab-org/security-products/analyzers/container-scanning)
images.

The default branch pipeline is scheduled and, like all scheduled pipelines,
publishes an addtional `:YYYYMMDDHHmm` tag:

* `registry.gitlab.com/gitlab-org/security-products/dependencies/trivy-db-glad:2`
* `registry.gitlab.com/gitlab-org/security-products/dependencies/trivy-db-glad:2022041317`

For all commits, the commit SHA and the branch name are published, for example:

* `registry.gitlab.com/gitlab-org/security-products/dependencies/trivy-db-glad:main`
* `registry.gitlab.com/gitlab-org/security-products/dependencies/trivy-db-glad:d7d1bce9d73d1bcf65d4aae9fe93d51370a03b33`
